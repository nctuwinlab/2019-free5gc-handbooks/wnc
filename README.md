# WNC 2019 期末報告

![Build Status](https://gitlab.com/pages/gitbook/badges/master/build.svg)

---

在本報告中包含以下的內容。

- [前言](Overview.md)
- [1. free5GC 容器化設計](1-free5GC-containerization-design.md)
- [2. free5GC v1.0 Docker 映像檔](2-free5GC-v1.0-docker-images.md)
- [3. 部署 free5GC CNFs 於 Kubernetes 上之操作](3-Deploy-free5GC-CNFs-on-K8s.md)

