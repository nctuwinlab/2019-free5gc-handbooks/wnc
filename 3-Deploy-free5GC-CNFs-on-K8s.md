# 3. 部署 free5GC CNFs 於 Kubernetes 上之操作
---

依據 [free5GC Installation Guide](https://www.free5gc.org/cluster) 第一部分的 About MongoDB 上的說明，我們可以知道需要先部署 Mongo DB，  
因此，在我們需要先部署 Mongo DB 。而為了需要資料可以永久儲存，我們將 MONgo DB 的資料儲存在 NFS Server 中。  
而為了要透過相關的 interface 來輸入資料到 Mongo DB ，我們也先事先先啟動 Web UI。  
  
而在往 [free5GC Installation Guide](https://www.free5gc.org/cluster) 的 Install AMF, SMF, HSS, PCRF 說明，我們可以知道再 free5GC 會需要使用到 kernel ，因此會需要改 kubelet 的權限。  
  
而在 [free5GC Installation Guide](https://www.free5gc.org/cluster) 的架構圖中，我們可以知道，AMF 和 UPF 都會需要兩個 interface ，在本報告中，我們採用 Intel 且有支援 SR-IOV 的網卡來新增 Virture Function (VF) 給 AMF 和 UPF 使用，也因此這邊會需要用到 SR-IOV CNI pulgin 與 Multus CNI pulgin。  
  
之後就是建立一個一個 free5GC 的 CNFs 了。  
  
因此整個的流程如下：  
  
1. 建立 Kubernetes Cluster 並安裝 Calico 與 Multus CNI plugin 
2. 將 SR-IOV VF 功能開啟並指定連接在 enodeB netwrok interface 的 VF 數量
3. 更新 Kubernetes 的 Work Node 的 kubelet 環境設定檔並重新 restart kubelet service
4. 安裝 NFS Server
5. 安裝 helm
6. free5GC 網路相關設定
7. 創立一個 free5GC 的 Kuberentes namespace 
8. SR-IOV 相關設定
9. 部署 Mongo DB 在 Kubernetes 上
10. 部署 free5GC Web UI 在 Kubernetes 上
11. 部署 free5GC CNFs 在 Kuberentes 上

而這樣的部署架構就如同下方的圖一樣  
![](https://i.imgur.com/w4Rt3bV.png)  

另外，在網路的架構如下圖所示。  
![](https://i.imgur.com/qsb5gRN.png)  


  
以下會依據每一步驟詳細說明。  

## 1. 建立 Kubernetes Cluster 並安裝 Calico 與 Multus CNI plugin
  
請參考 [Kubernetes 官網](https://kubernetes.io/docs/setup/) 或 [kubespray](https://github.com/kubernetes-sigs/kubespray) setup 一個 Kubernetes Cluster ，並且安裝 Calico 與 Multus CNI plugin  

## 2. 將 SR-IOV VF 功能開啟並指定連接在 enodeB netwrok interface 的 VF 數量

請參考 Intel 官網  

## 3. 更新 Kubernetes 的 Work Node 的 kubelet 環境設定檔並重新 restart kubelet service

更新 Kubernetes 每一台 Work Node 中的 `/etc/kubernetes/kubelet.env` 檔案  
  
在其中一行加入 `--allowed-unsafe-sysctls 'kernel.msg*,net.ipv4.*,net.ipv6.*' \` ，存檔離開。  
然後輸入指令 `sudo systemctl restart kubelet && sudo systemctl status kubelet` 重新 restart kubelet service  

## 4. 安裝 NFS Server

在 Kubernete 的 其中一台 Master Node 上安裝 NFS Server   

```
sudo apt-get install -qqy nfs-kernel-server
sudo mkdir -p /nfsshare/mongodb
echo "/nfsshare *(rw,sync,no_root_squash)" | sudo tee /etc/exports
sudo exportfs -r
sudo showmount -e
```

## 5. 安裝 helm


在 Kubernete 的 Master Node 上輸入以下指令，以後備用。

```
curl -L https://storage.googleapis.com/kubernetes-helm/helm-v2.9.1-linux-amd64.tar.gz > helm-v2.9.1-linux-amd64.tar.gz && tar -zxvf helm-v2.9.1-linux-amd64.tar.gz && chmod +x linux-amd64/helm && sudo mv linux-amd64/helm /usr/local/bin/helm
rm -rf /home/$USER/helm-v2.9.1-linux-amd64.tar.gz
sudo pip install yq
helm init
kubectl create serviceaccount --namespace kube-system tiller
kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
kubectl patch deploy --namespace kube-system tiller-deploy -p '{"spec":{"template":{"spec":{"serviceAccount":"tiller"}}}}'
```

## 6. free5GC 網路相關設定


### 1. 設定 free5gc.network

```
sudo sh -c "cat << EOF > /etc/systemd/network/99-free5gc.netdev
[NetDev]
Name=uptun
Kind=tun
EOF"

sudo systemctl enable systemd-networkd
sudo systemctl restart systemd-networkd

sudo sh -c "echo 'net.ipv6.conf.uptun.disable_ipv6=0' > /etc/sysctl.d/30-free5gc.conf"
sudo sysctl -p /etc/sysctl.d/30-free5gc.conf
sudo sh -c "cat << EOF > /etc/systemd/network/99-free5gc.network
[Match]
Name=uptun
[Network]
Address=45.45.0.1/16
EOF"
sudo systemctl enable systemd-networkd
sudo systemctl restart systemd-networkd
sudo sh -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'
```

Add iptables rules  
```
sudo iptables -t nat -A POSTROUTING -o ens3 -j MASQUERADE
sudo iptables -I INPUT -i uptun -j ACCEPT
```
PS. ens3 是連到 internet 的網路interface ，和 calico 是同一個 interface ，所以要改成和 calico 同一個 intetface 的 network intertface 名稱

### 2. Add the following lines into `/etc/network/interfaces` for network-manager service

```
auto uptun
iface uptun inet static
    address 45.45.0.1
    netmask 255.255.0.0

iface uptun inet6 static
    pre-up modprobe ipv6
    address cafe::1
    netmask 64
```

Restart uptun interface

```
sudo ip a flush uptun
sudo systemctl restart networking

# Check if uptun is up
sudo apt-get -y install net-tools
ifconfig uptun
```

## 7. 創立一個 free5GC 的 Kuberentes namespace·

```
cat <<EOF | kubectl create -f -
apiVersion: v1
kind: Namespace
metadata:
  name: free5gc
EOF
```

## 8. SR-IOV 相關設定

### 1. 設定 VF 數量在 SR-IOV device 並 創建網路 CRD 與 SR-IOV resource

先設定 VF 數量在 SR-IOV device (這部分先確認你網卡 interface 原本可以指定的 NF 數量)  

PS. Network insterface 名稱請修改成符合自己的  

在 kubernetes work node 執行以下指令：
```
$ ip a
5: ens11f3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq portid 8cea1b30da42 state UP group default qlen 1000
    link/ether 8c:ea:1b:30:da:42 brd ff:ff:ff:ff:ff:ff
    inet 192.188.2.99/24 brd 192.188.2.255 scope global ens11f3
       valid_lft forever preferred_lft forever
    inet6 fe80::8eea:1bff:fe30:da42/64 scope link
       valid_lft forever preferred_lft forever
$ echo 30 | sudo tee -a /sys/class/net/ens11f3/device/sriov_numvfs
```

接著在 kubernetes master node 創建網路的 CRD  
```
cat <<EOF | kubectl create -f -
apiVersion: apiextensions.k8s.io/v1beta1
kind: CustomResourceDefinition
metadata:
  name: network-attachment-definitions.k8s.cni.cncf.io
spec:
  group: k8s.cni.cncf.io
  version: v1
  scope: Namespaced
  names:
    plural: network-attachment-definitions
    singular: network-attachment-definition
    kind: NetworkAttachmentDefinition
    shortNames:
    - net-attach-def
  validation:
    openAPIV3Schema:
      properties:
        spec:
          properties:
            config:
                 type: string
EOF
```
因為創建好 network-attachment-definitions 這個 resource 了，所以在 kubernetes master node 創建 SR-IOV 的 network-attachment-definitions resource  
PS. Network insterface 名稱與網斷請修改成符合自己的  

```
cat <<EOF | kubectl create -f -
apiVersion: "k8s.cni.cncf.io/v1"
kind: NetworkAttachmentDefinition
metadata:
  name: sriov-net1
  namespace: free5gc
  annotations:
    k8s.v1.cni.cncf.io/resourceName: intel.com/sriov_net
spec:
  config: '{
  "type": "sriov",
  "master": "ens11f3",
  "name": "sriov-network",
  "ipam": {
    "type": "host-local",
    "subnet": "192.188.2.0/24",
    "rangeStart": "192.188.2.100",
    "rangeEnd": "192.188.2.200",
    "routes": [{
      "dst": "0.0.0.0/0"
    }],
    "gateway": "192.188.2.254"
  }
}'
---
apiVersion: "k8s.cni.cncf.io/v1"
kind: NetworkAttachmentDefinition
metadata:
  name: sriov-net1
  annotations:
    k8s.v1.cni.cncf.io/resourceName: intel.com/sriov_net
spec:
  config: '{
  "type": "sriov",
  "master": "ens11f3",
  "name": "sriov-network",
  "ipam": {
    "type": "host-local",
    "subnet": "192.188.2.0/24",
    "rangeStart": "192.188.2.100",
    "rangeEnd": "192.188.2.200",
    "routes": [{
      "dst": "0.0.0.0/0"
    }],
    "gateway": "192.188.2.254"
  }
}'
EOF
```

```
$ kubectl get net-attach-def
NAME                   AGE
sriov-net1             4h
```

### 2. 建立與執行 SRIOV network device plugin

Ref: https://github.com/intel/sriov-network-device-plugin#build-and-run-sriov-network-device-plugin

在 Kubernetes master node 執行以下指令：
```
$ git clone https://github.com/intel/sriov-network-device-plugin.git && cd sriov-network-device-plugin
$ git checkout v2.1.0
$ make && make image
```

### 3. 設定 /etc/pcidp/config.json

在 kubernetes work node 編輯 `/etc/pcidp/config.json`
PS. 請先使用 `lspci | grep -i Ethernet` 指令確認 interface 的 PCI address
檔案格式如下
```
{
    "resourceList":
    [
        {
            "resourceName": "sriov_net",
            "rootDevices": ["01:00.3"],
            "sriovMode": true,
            "deviceType": "netdevice"
        }
    ]
}
```

### 4. 部署 sriov-network-device-plugin 在 Kubernetes 上

在 kubernetes master node 上 執行以下指令

```
cat <<EOF | kubectl create -f -
# https://github.com/intel/sriov-network-device-plugin/blob/master/deployments/pod-sriovdp.yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: sriov-device-plugin
  labels:
    app: sriovdp
spec:
  replicas: 1
  selector:
    matchLabels:
      app: sriovdp
  template:
    metadata:
      labels:
        app: sriovdp
    spec:
      containers:
      - name: sriov-device-plugin
        image: nfvpe/sriov-device-plugin:latest
        imagePullPolicy: IfNotPresent
        command: [ "/bin/sh", "-c", "--" ]
        args: [ "sriovdp --logtostderr 10;" ]
        volumeMounts:
        - mountPath: /var/lib/kubelet/device-plugins/
          name: devicesock
          readOnly: false
        - mountPath: /sys
          name: net
          readOnly: true
        - mountPath: /etc/pcidp/
          name: config
          readOnly: true
      volumes:
        - name: devicesock
          hostPath:
          # directory location on host
            path: /var/lib/kubelet/device-plugins/
        - name: net
          hostPath:
            path: /sys
        - name: config
          hostPath:
            path: /etc/pcidp/
      hostNetwork: true
      hostPID: true
EOF
```
部署完後， 可以在 Kubernetes master node 上使用以下指令確認是否成功

```
$ kubectl get node WORK_NODE_NAME -o json | jq '.status.allocatable'
{
  "cpu": "31800m",
  "ephemeral-storage": "226240619760",
  "hugepages-1Gi": "8Gi",
  "intel.com/sriov_net": "30",
  "memory": "56931140Ki",
  "pods": "110"
}
```

## 9. 部署 Mongo DB 在 Kubernetes 上

在 kubernets Master node 上執行以下指令  

```
$ export NFS_SERVER_IP=XXX.XXX.XXX.XXX
$ cat <<EOF | kubectl create -f -
apiVersion: v1
kind: PersistentVolume
metadata:
  name: mongo-nfs
spec:
  storageClassName: mongo
  capacity:
    storage: 20Gi
  accessModes:
  - ReadWriteMany
  nfs:
    path: /nfsshare
    server: ${NFS_SERVER_IP}
  persistentVolumeReclaimPolicy: Recycle
EOF

$ cat <<EOF | kubectl create -f -
apiVersion: apps/v1beta1
kind: StatefulSet
metadata:
  name: mongo
  namespace: free5gc
spec:
  serviceName: "mongo"
  replicas: 1
  template:
    metadata:
      labels:
        service: mongo
        role: db
    spec:
      terminationGracePeriodSeconds: 10
      containers:
        - name: nextepc-mongodb
          resources:
            requests:
              cpu: 100m
          image: mongo:4.1-xenial
          command:
            - mongod
            - "--bind_ip"
            - 0.0.0.0
          ports:
            - containerPort: 27017
          volumeMounts:
            - name: mongodb
              mountPath: /data/db
  volumeClaimTemplates:
  - metadata:
      name: mongodb
    spec:
      accessModes: [ "ReadWriteMany" ]
      storageClassName: mongo
      resources:
        requests:
          storage: 10Gi
EOF

$ cat <<EOF | kubectl create -f -
apiVersion: v1
kind: Service
metadata:
  labels:
    environment: testing
    service: mongo
  name: mongo-external
  namespace: free5gc
spec:
  externalTrafficPolicy: Cluster
  ports:
  - name: mongo
    nodePort: 31717
    port: 27017
    protocol: TCP
    targetPort: 27017
  selector:
    service: mongo
  sessionAffinity: None
  type: NodePort
EOF
```

## 10. 部署 free5GC Web UI 在 Kubernetes 上

在 kubernets Master node 上執行以下指令  
```
$ cat <<EOF | kubectl create -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: webui-deployment
  namespace: free5gc
  labels:
    app: webui
spec:
  replicas: 1
  selector:
    matchLabels:
      app: webui
  template:
    metadata:
      labels:
        app: webui
    spec:
      containers:
      - name: nextepc-webui
        image: sufuf3/nextepc-webui:latest
        command:
          - npm
          - run
          - dev
        imagePullPolicy: IfNotPresent
        ports:
        - containerPort: 3000
        env:
          - name: DB_URI
            value: mongodb://mongo-external.free5gc.svc.cluster.local/nextepc
EOF
$ cat <<EOF | kubectl create -f -
apiVersion: v1
kind: Service
metadata:
  labels:
    environment: testing
    app: webui
  name: webui-np
  namespace: free5gc
spec:
  externalTrafficPolicy: Cluster
  ports:
  - name: webui
    nodePort: 31727
    port: 3000
    protocol: TCP
    targetPort: 3000
  selector:
    app: webui
  sessionAffinity: None
  type: NodePort
EOF
```

當 Web UI 部署好後，就可以透過 web browser.access NODE_IP:31727。帳號 admin ：密碼 1423 來 輸入電信業者的 SIM 卡資訊。

## 11. 部署 free5GC CNFs 在 Kuberentes 上

在 kubernets Master node 上執行以下指令  

5 個 CNFs 的 IP 分配如下：  

- amf:
  - addr: 10.233.100.202
  - s1ap: 192.188.2.100
- hss:
   - addr: 10.233.100.203
- smf:
   - addr: 10.233.100.204
- pcrf:
   - addr: 10.233.100.205
- upf:
   - addr: 10.233.100.206
   - gtpu: 192.188.2.101

> 10.233.X.X 是 CNFs 互相溝通的介面，是 calico 的 interface  
> 192.188.2.X 是和 enodeB 對接的 VF 

如圖所示  

![](https://i.imgur.com/3JUCkw8.png)

### 1. 設定 free5gc-configmap

```
cat <<EOF | kubectl create -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: free5gc-config
  namespace: free5gc
data:
  config.file: |
    ### For reference, see `free5gc.conf`
    db_uri: mongodb://mongo-external.free5gc.svc.cluster.local/free5gc
    logger:
      file: /usr/src/free5gc/install/var/log/free5gc/free5gc.log
      trace:
        app: 1
        s1ap: 1
        nas: 1
        diameter: 1
        gtp: 1
        pfcp: 1
        sbi: 1
    #
    # parameter:
    #
    #  o Number of output streams per SCTP associations.
    #      sctp_streams: 30
    #
    #  o Disable use of IPv4 addresses (only IPv6)
    #      no_ipv4: true
    #
    #  o Disable use of IPv6 addresses (only IPv4)
    #      no_ipv6: true
    #
    #  o Prefer IPv4 instead of IPv6 for estabishing new GTP connections.
    #      prefer_ipv4: true
    #
    #  o Enable Multicast traffic to the UE
    #      multicast: true
    #
    #  o Disable Stateless Address Autoconfiguration for IPv6
    #      no_slaac: true
    #
    #
    parameter:
        no_ipv6: true
    amf:
        freeDiameter: amf.conf
    #
    #  <S1AP Server>>
    #
    #  o S1AP Server(all address avaiable)
    #    s1ap:
    #
    #  o S1AP Server(0.0.0.0:36412)
    #    s1ap:
    #      addr: 0.0.0.0
    #
    #  o S1AP Server(127.0.0.1:36412, [::1]:36412)
    #    s1ap:
    #      - addr: 127.0.0.1
    #      - addr: ::1
    #  o S1AP Server(different port)
    #    s1ap:
    #      - addr: 127.0.0.1
    #        port: 36413
    #
    #  o S1AP Server(address avaiable in `eth0` interface)
    #    s1ap:
    #      dev: eth0
    #
        s1ap:
          addr: 192.188.2.100
    #
    #  <GUMMEI>
    #
    #  o Multiple GUMMEI
    #    gummei:
    #      - plmn_id:
    #          mcc: 001
    #          mnc: 01
    #        mme_gid: 2
    #        mme_code: 1
    #      - plmn_id:
    #          - mcc: 002
    #            mnc: 02
    #          - mcc: 003
    #            mnc: 03
    #        mme_gid: [3, 4]
    #        mme_code:
    #          - 2
    #          - 3
    #
        gummei:
          plmn_id:
            mcc: 208
            mnc: 93
          mme_gid: 1
          mme_code: 1
    #
    #  <TAI>
    #
    #  o Multiple TAI
    #    tai:
    #      - plmn_id:
    #          mcc: 001
    #          mnc: 01
    #        tac: [1, 2, 3]
    #    tai:
    #      - plmn_id:
    #          mcc: 002
    #          mnc: 02
    #        tac: 4
    #      - plmn_id:
    #          mcc: 003
    #          mnc: 03
    #        tac: 5
    #    tai:
    #      - plmn_id:
    #          mcc: 004
    #          mnc: 04
    #        tac: [6, 7]
    #      - plmn_id:
    #          mcc: 005
    #          mnc: 05
    #        tac: 8
    #      - plmn_id:
    #          mcc: 006
    #          mnc: 06
    #        tac: [9, 10]
    #
        tai:
          plmn_id:
            mcc: 208
            mnc: 93
          tac: 1
        security:
            integrity_order : [ EIA1, EIA2, EIA0 ]
            ciphering_order : [ EEA0, EEA1, EEA2 ]
    #
    #  <Network Name>
    #    network_name:
    #        full: free5GC
    #        short: free
    #
        network_name:
            full: free5GC
    hss:
        freeDiameter: hss.conf
    pcrf:
        freeDiameter: pcrf.conf
    smf:
        freeDiameter: smf.conf
        pfcp:
          - addr: 10.233.100.204
          - addr: ::1
        upf:
          - addr: 10.233.100.206
        http:
          addr: 10.233.100.204
          port: 8080
    #
    #  <UE Pool>
    #
    #  o IPv4 Pool
    #    $ sudo ip addr add 45.45.0.1/16 dev uptun
    #
    #    ue_pool:
    #      addr: 45.45.0.1/16
    #
    #  o IPv4/IPv6 Pool
    #    $ sudo ip addr add 45.45.0.1/16 dev uptun
    #    $ sudo ip addr add cafe:1::1/64 dev uptun
    #
    #    ue_pool:
    #      - addr: 45.45.0.1/16
    #      - addr: cafe:1::1/64
    #
    #
    #  o Specific APN(e.g 'volte') uses 45.46.0.1/16, cafe:2::1/64
    #    All other APNs use 45.45.0.1/16, cafe:1::1/64
    #    $ sudo ip addr add 45.45.0.1/16 dev uptun
    #    $ sudo ip addr add 45.46.0.1/16 dev uptun
    #    $ sudo ip addr add cafe:1::1/64 dev uptun
    #    $ sudo ip addr add cafe:2::1/64 dev uptun
    #
    #    ue_pool:
    #      - addr: 45.45.0.1/16
    #      - addr: cafe:1::1/64
    #      - addr: 45.46.0.1/16
    #        apn: volte
    #      - addr: cafe:2::1/64
    #        apn: volte
    #
    #  o Multiple Devices (default: uptun)
    #    $ sudo ip addr add 45.45.0.1/16 dev uptun
    #    $ sudo ip addr add cafe:1::1/64 dev uptun2
    #    $ sudo ip addr add 45.46.0.1/16 dev uptun3
    #    $ sudo ip addr add cafe:2::1/64 dev uptun3
    #
    #    ue_pool:
    #      - addr: 45.45.0.1/16
    #      - addr: cafe:1::1/64
    #        dev: uptun2
    #      - addr: 45.46.0.1/16
    #        apn: volte
    #        dev: uptun3
    #      - addr: cafe:2::1/64
    #        apn: volte
    #        dev: uptun3
    #
        ue_pool:
          - addr: 45.45.0.1/16
          - addr: cafe::1/64
    #
    #  <Domain Name Server>
    #
    #  o Primary/Secondary can be configured. Others are ignored.
    #
        dns:
          - 8.8.8.8
          - 8.8.4.4
          - 2001:4860:4860::8888
          - 2001:4860:4860::8844
    upf:
        pfcp:
          addr: 10.233.100.206
    #
    #  <GTP-U Server>>
    #
    #  o GTP-U Server(127.0.0.3:2152, [::1]:2152)
    #    gtpu:
    #      - addr: 127.0.0.3
    #      - addr: ::1
    #
    #  o Same configuration(127.0.0.3:2152, [::1]:2152) as below.
    #    gtpu:
    #      name: localhost
    #
        gtpu:
          - addr: 192.188.2.101
          - addr: ::1
        ue_pool:
          - addr: 45.45.0.1/16
          - addr: cafe::1/64
        dns:
          - 8.8.8.8
          - 8.8.4.4
          - 2001:4860:4860::8888
          - 2001:4860:4860::8844
EOF
```

### 2. 部署 AMF 在 Kubernetes 上

#### 1. 設定 `deploy/nctu5GC/amf-freediameter-configmap.yaml`

請參考 https://hackmd.io/s/S1baJOeEE#Setup-AMF

```
cat <<EOF | kubectl create -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: freediameter-amf-config
  namespace: free5gc
data:
  config.file: |
    # This is a sample configuration file for freeDiameter daemon.

    # Most of the options can be omitted, as they default to reasonable values.
    # Only TLS-related options must be configured properly in usual setups.

    # It is possible to use "include" keyword to import additional files
    # e.g.: include "/etc/freeDiameter.d/*.conf"
    # This is exactly equivalent as copy & paste the content of the included file(s)
    # where the "include" keyword is found.


    ##############################################################
    ##  Peer identity and realm

    # The Diameter Identity of this daemon.
    # This must be a valid FQDN that resolves to the local host.
    # Default: hostname's FQDN
    #Identity = "aaa.koganei.freediameter.net";
    Identity = "amf.localdomain";

    # The Diameter Realm of this daemon.
    # Default: the domain part of Identity (after the first dot).
    #Realm = "koganei.freediameter.net";
    Realm = "localdomain";

    ##############################################################
    ##  Transport protocol configuration

    # The port this peer is listening on for incoming connections (TCP and SCTP).
    # Default: 3868. Use 0 to disable.
    #Port = 3868;

    # The port this peer is listening on for incoming TLS-protected connections (TCP and SCTP).
    # See TLS_old_method for more information about TLS flavours.
    # Note: we use TLS/SCTP instead of DTLS/SCTP at the moment. This will change in future version of freeDiameter.
    # Default: 5868. Use 0 to disable.
    #SecPort = 5868;

    # Use RFC3588 method for TLS protection, where TLS is negociated after CER/CEA exchange is completed
    # on the unsecure connection. The alternative is RFC6733 mechanism, where TLS protects also the
    # CER/CEA exchange on a dedicated secure port.
    # This parameter only affects outgoing connections.
    # The setting can be also defined per-peer (see Peers configuration section).
    # Default: use RFC6733 method with separate port for TLS.
    #TLS_old_method;

    # Disable use of TCP protocol (only listen and connect over SCTP)
    # Default : TCP enabled
    #No_TCP;

    # Disable use of SCTP protocol (only listen and connect over TCP)
    # Default : SCTP enabled
    #No_SCTP;
    No_SCTP;
    # This option is ignored if freeDiameter is compiled with DISABLE_SCTP option.

    # Prefer TCP instead of SCTP for establishing new connections.
    # This setting may be overwritten per peer in peer configuration blocs.
    # Default : SCTP is attempted first.
    #Prefer_TCP;

    # Default number of streams per SCTP associations.
    # This setting may be overwritten per peer basis.
    # Default : 30 streams
    #SCTP_streams = 30;

    ##############################################################
    ##  Endpoint configuration

    # Disable use of IP addresses (only IPv6)
    # Default : IP enabled
    #No_IP;

    # Disable use of IPv6 addresses (only IP)
    # Default : IPv6 enabled
    #No_IPv6;

    # Specify local addresses the server must bind to
    # Default : listen on all addresses available.
    #ListenOn = "202.249.37.5";
    #ListenOn = "2001:200:903:2::202:1";
    #ListenOn = "fe80::21c:5ff:fe98:7d62%eth0";
    ListenOn = "10.233.100.202";


    ##############################################################
    ##  Server configuration

    # How many Diameter peers are allowed to be connecting at the same time ?
    # This parameter limits the number of incoming connections from the time
    # the connection is accepted until the first CER is received.
    # Default: 5 unidentified clients in paralel.
    #ThreadsPerServer = 5;

    ##############################################################
    ##  TLS Configuration

    # TLS is managed by the GNUTLS library in the freeDiameter daemon.
    # You may find more information about parameters and special behaviors
    # in the relevant documentation.
    # http://www.gnu.org/software/gnutls/manual/

    # Credentials of the local peer
    # The X509 certificate and private key file to use for the local peer.
    # The files must contain PKCS-1 encoded RSA key, in PEM format.
    # (These parameters are passed to gnutls_certificate_set_x509_key_file function)
    # Default : NO DEFAULT
    #TLS_Cred = "<x509 certif file.PEM>" , "<x509 private key file.PEM>";
    #TLS_Cred = "/etc/ssl/certs/freeDiameter.pem", "/etc/ssl/private/freeDiameter.key";
    TLS_Cred = "/usr/src/free5gc/install/etc/free5gc/freeDiameter/amf.cert.pem", "/usr/src/free5gc/install/etc/free5gc/freeDiameter/amf.key.pem";

    # Certificate authority / trust anchors
    # The file containing the list of trusted Certificate Authorities (PEM list)
    # (This parameter is passed to gnutls_certificate_set_x509_trust_file function)
    # The directive can appear several times to specify several files.
    # Default : GNUTLS default behavior
    #TLS_CA = "<file.PEM>";
    TLS_CA = "/usr/src/free5gc/install/etc/free5gc/freeDiameter/cacert.pem";

    # Certificate Revocation List file
    # The information about revoked certificates.
    # The file contains a list of trusted CRLs in PEM format. They should have been verified before.
    # (This parameter is passed to gnutls_certificate_set_x509_crl_file function)
    # Note: openssl CRL format might have interoperability issue with GNUTLS format.
    # Default : GNUTLS default behavior
    #TLS_CRL = "<file.PEM>";

    # GNU TLS Priority string
    # This string allows to configure the behavior of GNUTLS key exchanges
    # algorithms. See gnutls_priority_init function documentation for information.
    # You should also refer to the Diameter required TLS support here:
    #   http://tools.ietf.org/html/rfc6733#section-13.1
    # Default : "NORMAL"
    # Example: TLS_Prio = "NONE:+VERS-TLS1.1:+AES-128-CBC:+RSA:+SHA1:+COMP-NULL";
    #TLS_Prio = "NORMAL";

    # Diffie-Hellman parameters size
    # Set the number of bits for generated DH parameters
    # Valid value should be 768, 1024, 2048, 3072 or 4096.
    # (This parameter is passed to gnutls_dh_params_generate2 function,
    # it usually should match RSA key size)
    # Default : 1024
    #TLS_DH_Bits = 1024;

    # Alternatively, you can specify a file to load the PKCS#3 encoded
    # DH parameters directly from. This accelerates the daemon start
    # but is slightly less secure. If this file is provided, the
    # TLS_DH_Bits parameters has no effect.
    # Default : no default.
    #TLS_DH_File = "<file.PEM>";


    ##############################################################
    ##  Timers configuration

    # The Tc timer of this peer.
    # It is the delay before a new attempt is made to reconnect a disconnected peer.
    # The value is expressed in seconds. The recommended value is 30 seconds.
    # Default: 30
    #TcTimer = 30;

    # The Tw timer of this peer.
    # It is the delay before a watchdog message is sent, as described in RFC 3539.
    # The value is expressed in seconds. The default value is 30 seconds. Value must
    # be greater or equal to 6 seconds. See details in the RFC.
    # Default: 30
    #TwTimer = 30;

    ##############################################################
    ##  Applications configuration

    # Disable the relaying of Diameter messages?
    # For messages not handled locally, the default behavior is to forward the
    # message to another peer if any is available, according to the routing
    # algorithms. In addition the "0xffffff" application is advertised in CER/CEA
    # exchanges.
    # Default: Relaying is enabled.
    #NoRelay;

    # Number of server threads that can handle incoming messages at the same time.
    # Default: 4
    #AppServThreads = 4;

    # Other applications are configured by loaded extensions.

    ##############################################################
    ##  Extensions configuration

    #  The freeDiameter framework merely provides support for
    # Diameter Base Protocol. The specific application behaviors,
    # as well as advanced functions, are provided
    # by loadable extensions (plug-ins).
    #  These extensions may in addition receive the name of a
    # configuration file, the format of which is extension-specific.
    #
    # Format:
    #LoadExtension = "/path/to/extension" [ : "/optional/configuration/file" ] ;
    #
    # Examples:
    #LoadExtension = "extensions/sample.fdx";
    #LoadExtension = "extensions/sample.fdx":"conf/sample.conf";

    # Extensions are named as follow:
    # dict_* for extensions that add content to the dictionary definitions.
    # dbg_*  for extensions useful only to retrieve more information on the framework execution.
    # acl_*  : Access control list, to control which peers are allowed to connect.
    # rt_*   : routing extensions that impact how messages are forwarded to other peers.
    # app_*  : applications, these extensions usually register callbacks to handle specific messages.
    # test_* : dummy extensions that are useful only in testing environments.


    # The dbg_msg_dump.fdx extension allows you to tweak the way freeDiameter displays some
    # information about some events. This extension does not actually use a configuration file
    # but receives directly a parameter in the string passed to the extension. Here are some examples:
    ## LoadExtension = "dbg_msg_dumps.fdx" : "0x1111"; # Removes all default hooks, very quiet even in case of errors.
    ## LoadExtension = "dbg_msg_dumps.fdx" : "0x2222"; # Display all events with few details.
    ## LoadExtension = "dbg_msg_dumps.fdx" : "0x0080"; # Dump complete information about sent and received messages.
    # The four digits respectively control: connections, routing decisions, sent/received messages, errors.
    # The values for each digit are:
    #  0 - default - keep the default behavior
    #  1 - quiet   - remove any specific log
    #  2 - compact - display only a summary of the information
    #  4 - full    - display the complete information on a single long line
    #  8 - tree    - display the complete information in an easier to read format spanning several lines.

    LoadExtension = "dbg_msg_dumps.so" : "0x8888";
    LoadExtension = "dict_rfc5777.so";
    LoadExtension = "dict_mip6i.so";
    LoadExtension = "dict_nasreq.so";
    LoadExtension = "dict_nas_mipv6.so";
    LoadExtension = "dict_dcca.so";
    LoadExtension = "dict_dcca_3gpp.so";
    LoadExtension = "dict_s6a.so";


    ##############################################################
    ##  Peers configuration

    #  The local server listens for incoming connections. By default,
    # all unknown connecting peers are rejected. Extensions can override this behavior (e.g., acl_wl).
    #
    #  In addition to incoming connections, the local peer can
    # be configured to establish and maintain connections to some
    # Diameter nodes and allow connections from these nodes.
    #  This is achieved with the ConnectPeer directive described below.
    #
    # Note that the configured Diameter Identity MUST match
    # the information received inside CEA, or the connection will be aborted.
    #
    # Format:
    #ConnectPeer = "diameterid" [ { parameter1; parameter2; ...} ] ;
    # Parameters that can be specified in the peer's parameter list:
    #  No_TCP; No_SCTP; No_IP; No_IPv6; Prefer_TCP; TLS_old_method;
    #  No_TLS;       # assume transparent security instead of TLS. DTLS is not supported yet (will change in future versions).
    #  Port = 5868;  # The port to connect to
    #  TcTimer = 30;
    #  TwTimer = 30;
    #  ConnectTo = "202.249.37.5";
    #  ConnectTo = "2001:200:903:2::202:1";
    #  TLS_Prio = "NORMAL";
    #  Realm = "realm.net"; # Reject the peer if it does not advertise this realm.
    # Examples:
    #ConnectPeer = "aaa.wide.ad.jp";
    #ConnectPeer = "old.diameter.serv" { TcTimer = 60; TLS_old_method; No_SCTP; Port=3868; } ;
    # HSS IP
    ConnectPeer = "hss.localdomain" { ConnectTo = "10.233.100.203"; No_TLS; };

    ##############################################################
EOF
```

#### 2. 部署 AMF 在 Kubernetes 上

```
cat <<EOF | kubectl create -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: amf-deployment
  namespace: free5gc
  labels:
    app: amf
spec:
  replicas: 1
  selector:
    matchLabels:
      app: amf
  template:
    metadata:
      labels:
        app: amf
      annotations:
        cni.projectcalico.org/ipAddrs: "[\"10.233.100.202\"]"
        k8s.v1.cni.cncf.io/networks: sriov-net1
    spec:
      hostname: amf
      subdomain: localdomain
      hostAliases:
      - ip: "10.233.100.202"
        hostnames:
        - "amf.localdomain"
      - ip: "10.233.100.203"
        hostnames:
        - "hss.localdomain"
      - ip: "10.233.100.204"
        hostnames:
        - "smf.localdomain"
      - ip: "10.233.100.205"
        hostnames:
        - "pcrf.localdomain"
      - ip: "10.233.100.206"
        hostnames:
        - "upf.localdomain"
      containers:
      - name: amf-container
        image: sufuf3/nextepc-build:latest
        command: ["/bin/bash"]
        args: ["-c", "ip addr add 192.188.2.100/24 dev net1 && /usr/src/free5gc/free5gc-amfd"]
        ports:
          - containerPort: 5868
            name: api1
          - containerPort: 3868
            name: api2
        stdin: true
        tty: true
        imagePullPolicy: IfNotPresent
        env:
          - name: DB_URI
            value: mongodb://mongo-external.free5gc.svc.cluster.local/nextepc
        volumeMounts:
        - name: config-volume
          mountPath: /usr/src/free5gc/install/etc/free5gc/free5gc.conf
          subPath: free5gc.conf
        - name: freediameter-volume
          mountPath: /usr/src/free5gc/install/etc/free5gc/freeDiameter/amf.conf
          subPath: amf.conf
        securityContext:
          privileged: true
          capabilities:
            add: ["NET_ADMIN", "SYS_TIME"]
        resources:
          requests:
            intel.com/sriov_net: '1'
          limits:
            intel.com/sriov_net: '1'
      #hostNetwork: true
      securityContext:
        sysctls:
          - name: net.ipv6.conf.all.disable_ipv6
            value: "0"
      volumes:
        - name: config-volume
          configMap:
            name: free5gc-config
            items:
            - key: config.file
              path: free5gc.conf
        - name: freediameter-volume
          configMap:
            name: freediameter-amf-config
            items:
            - key: config.file
              path: amf.conf
EOF
```

### 3. 部署 HSS 在 Kubernetes 上

#### 1. 設定 HSS configmap

請參考 https://hackmd.io/s/S1baJOeEE#Setup-HSS

```
cat <<EOF | kubectl create -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: hss-config
  namespace: free5gc
data:
  config.file: |
    ### For reference, see `free5gc.conf`
    db_uri: mongodb://mongo-external.free5gc.svc.cluster.local/free5gc

    logger:
      file: /usr/src/free5gc/install/var/log/free5gc/hss.log
      trace:
        app: 1
        diameter: 1

    parameter:

    hss:
      freeDiameter: hss.conf
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: freediameter-hss-config
  namespace: free5gc
data:
  config.file: |
    # This is a sample configuration file for freeDiameter daemon.

    # Most of the options can be omitted, as they default to reasonable values.
    # Only TLS-related options must be configured properly in usual setups.

    # It is possible to use "include" keyword to import additional files
    # e.g.: include "/etc/freeDiameter.d/*.conf"
    # This is exactly equivalent as copy & paste the content of the included file(s)
    # where the "include" keyword is found.


    ##############################################################
    ##  Peer identity and realm

    # The Diameter Identity of this daemon.
    # This must be a valid FQDN that resolves to the local host.
    # Default: hostname's FQDN
    #Identity = "aaa.koganei.freediameter.net";
    Identity = "hss.localdomain";

    # The Diameter Realm of this daemon.
    # Default: the domain part of Identity (after the first dot).
    #Realm = "koganei.freediameter.net";
    Realm = "localdomain";

    ##############################################################
    ##  Transport protocol configuration

    # The port this peer is listening on for incoming connections (TCP and SCTP).
    # Default: 3868. Use 0 to disable.
    #Port = 3868;

    # The port this peer is listening on for incoming TLS-protected connections (TCP and SCTP).
    # See TLS_old_method for more information about TLS flavours.
    # Note: we use TLS/SCTP instead of DTLS/SCTP at the moment. This will change in future version of freeDiameter.
    # Default: 5868. Use 0 to disable.
    #SecPort = 5868;

    # Use RFC3588 method for TLS protection, where TLS is negociated after CER/CEA exchange is completed
    # on the unsecure connection. The alternative is RFC6733 mechanism, where TLS protects also the
    # CER/CEA exchange on a dedicated secure port.
    # This parameter only affects outgoing connections.
    # The setting can be also defined per-peer (see Peers configuration section).
    # Default: use RFC6733 method with separate port for TLS.
    #TLS_old_method;

    # Disable use of TCP protocol (only listen and connect over SCTP)
    # Default : TCP enabled
    #No_TCP;

    # Disable use of SCTP protocol (only listen and connect over TCP)
    # Default : SCTP enabled
    #No_SCTP;
    No_SCTP;
    # This option is ignored if freeDiameter is compiled with DISABLE_SCTP option.

    # Prefer TCP instead of SCTP for establishing new connections.
    # This setting may be overwritten per peer in peer configuration blocs.
    # Default : SCTP is attempted first.
    #Prefer_TCP;

    # Default number of streams per SCTP associations.
    # This setting may be overwritten per peer basis.
    # Default : 30 streams
    #SCTP_streams = 30;

    ##############################################################
    ##  Endpoint configuration

    # Disable use of IP addresses (only IPv6)
    # Default : IP enabled
    #No_IP;

    # Disable use of IPv6 addresses (only IP)
    # Default : IPv6 enabled
    #No_IPv6;

    # Specify local addresses the server must bind to
    # Default : listen on all addresses available.
    #ListenOn = "202.249.37.5";
    #ListenOn = "2001:200:903:2::202:1";
    #ListenOn = "fe80::21c:5ff:fe98:7d62%eth0";
    ListenOn = "10.233.100.203";


    ##############################################################
    ##  Server configuration

    # How many Diameter peers are allowed to be connecting at the same time ?
    # This parameter limits the number of incoming connections from the time
    # the connection is accepted until the first CER is received.
    # Default: 5 unidentified clients in paralel.
    #ThreadsPerServer = 5;

    ##############################################################
    ##  TLS Configuration

    # TLS is managed by the GNUTLS library in the freeDiameter daemon.
    # You may find more information about parameters and special behaviors
    # in the relevant documentation.
    # http://www.gnu.org/software/gnutls/manual/

    # Credentials of the local peer
    # The X509 certificate and private key file to use for the local peer.
    # The files must contain PKCS-1 encoded RSA key, in PEM format.
    # (These parameters are passed to gnutls_certificate_set_x509_key_file function)
    # Default : NO DEFAULT
    #TLS_Cred = "<x509 certif file.PEM>" , "<x509 private key file.PEM>";
    #TLS_Cred = "/etc/ssl/certs/freeDiameter.pem", "/etc/ssl/private/freeDiameter.key";
    TLS_Cred = "/usr/src/free5gc/install/etc/free5gc/freeDiameter/hss.cert.pem", "/usr/src/free5gc/install/etc/free5gc/freeDiameter/hss.key.pem";

    # Certificate authority / trust anchors
    # The file containing the list of trusted Certificate Authorities (PEM list)
    # (This parameter is passed to gnutls_certificate_set_x509_trust_file function)
    # The directive can appear several times to specify several files.
    # Default : GNUTLS default behavior
    #TLS_CA = "<file.PEM>";
    TLS_CA = "/usr/src/free5gc/install/etc/free5gc/freeDiameter/cacert.pem";

    # Certificate Revocation List file
    # The information about revoked certificates.
    # The file contains a list of trusted CRLs in PEM format. They should have been verified before.
    # (This parameter is passed to gnutls_certificate_set_x509_crl_file function)
    # Note: openssl CRL format might have interoperability issue with GNUTLS format.
    # Default : GNUTLS default behavior
    #TLS_CRL = "<file.PEM>";

    # GNU TLS Priority string
    # This string allows to configure the behavior of GNUTLS key exchanges
    # algorithms. See gnutls_priority_init function documentation for information.
    # You should also refer to the Diameter required TLS support here:
    #   http://tools.ietf.org/html/rfc6733#section-13.1
    # Default : "NORMAL"
    # Example: TLS_Prio = "NONE:+VERS-TLS1.1:+AES-128-CBC:+RSA:+SHA1:+COMP-NULL";
    #TLS_Prio = "NORMAL";

    # Diffie-Hellman parameters size
    # Set the number of bits for generated DH parameters
    # Valid value should be 768, 1024, 2048, 3072 or 4096.
    # (This parameter is passed to gnutls_dh_params_generate2 function,
    # it usually should match RSA key size)
    # Default : 1024
    #TLS_DH_Bits = 1024;

    # Alternatively, you can specify a file to load the PKCS#3 encoded
    # DH parameters directly from. This accelerates the daemon start
    # but is slightly less secure. If this file is provided, the
    # TLS_DH_Bits parameters has no effect.
    # Default : no default.
    #TLS_DH_File = "<file.PEM>";


    ##############################################################
    ##  Timers configuration

    # The Tc timer of this peer.
    # It is the delay before a new attempt is made to reconnect a disconnected peer.
    # The value is expressed in seconds. The recommended value is 30 seconds.
    # Default: 30
    #TcTimer = 30;

    # The Tw timer of this peer.
    # It is the delay before a watchdog message is sent, as described in RFC 3539.
    # The value is expressed in seconds. The default value is 30 seconds. Value must
    # be greater or equal to 6 seconds. See details in the RFC.
    # Default: 30
    #TwTimer = 30;

    ##############################################################
    ##  Applications configuration

    # Disable the relaying of Diameter messages?
    # For messages not handled locally, the default behavior is to forward the
    # message to another peer if any is available, according to the routing
    # algorithms. In addition the "0xffffff" application is advertised in CER/CEA
    # exchanges.
    # Default: Relaying is enabled.
    #NoRelay;

    # Number of server threads that can handle incoming messages at the same time.
    # Default: 4
    #AppServThreads = 4;

    # Other applications are configured by loaded extensions.

    ##############################################################
    ##  Extensions configuration

    #  The freeDiameter framework merely provides support for
    # Diameter Base Protocol. The specific application behaviors,
    # as well as advanced functions, are provided
    # by loadable extensions (plug-ins).
    #  These extensions may in addition receive the name of a
    # configuration file, the format of which is extension-specific.
    #
    # Format:
    #LoadExtension = "/path/to/extension" [ : "/optional/configuration/file" ] ;
    #
    # Examples:
    #LoadExtension = "extensions/sample.fdx";
    #LoadExtension = "extensions/sample.fdx":"conf/sample.conf";

    # Extensions are named as follow:
    # dict_* for extensions that add content to the dictionary definitions.
    # dbg_*  for extensions useful only to retrieve more information on the framework execution.
    # acl_*  : Access control list, to control which peers are allowed to connect.
    # rt_*   : routing extensions that impact how messages are forwarded to other peers.
    # app_*  : applications, these extensions usually register callbacks to handle specific messages.
    # test_* : dummy extensions that are useful only in testing environments.


    # The dbg_msg_dump.fdx extension allows you to tweak the way freeDiameter displays some
    # information about some events. This extension does not actually use a configuration file
    # but receives directly a parameter in the string passed to the extension. Here are some examples:
    ## LoadExtension = "dbg_msg_dumps.fdx" : "0x1111"; # Removes all default hooks, very quiet even in case of errors.
    ## LoadExtension = "dbg_msg_dumps.fdx" : "0x2222"; # Display all events with few details.
    ## LoadExtension = "dbg_msg_dumps.fdx" : "0x0080"; # Dump complete information about sent and received messages.
    # The four digits respectively control: connections, routing decisions, sent/received messages, errors.
    # The values for each digit are:
    #  0 - default - keep the default behavior
    #  1 - quiet   - remove any specific log
    #  2 - compact - display only a summary of the information
    #  4 - full    - display the complete information on a single long line
    #  8 - tree    - display the complete information in an easier to read format spanning several lines.

    LoadExtension = "dbg_msg_dumps.so" : "0x8888";
    LoadExtension = "dict_rfc5777.so";
    LoadExtension = "dict_mip6i.so";
    LoadExtension = "dict_nasreq.so";
    LoadExtension = "dict_nas_mipv6.so";
    LoadExtension = "dict_dcca.so";
    LoadExtension = "dict_dcca_3gpp.so";
    LoadExtension = "dict_s6a.so";


    ##############################################################
    ##  Peers configuration

    #  The local server listens for incoming connections. By default,
    # all unknown connecting peers are rejected. Extensions can override this behavior (e.g., acl_wl).
    #
    #  In addition to incoming connections, the local peer can
    # be configured to establish and maintain connections to some
    # Diameter nodes and allow connections from these nodes.
    #  This is achieved with the ConnectPeer directive described below.
    #
    # Note that the configured Diameter Identity MUST match
    # the information received inside CEA, or the connection will be aborted.
    #
    # Format:
    #ConnectPeer = "diameterid" [ { parameter1; parameter2; ...} ] ;
    # Parameters that can be specified in the peer's parameter list:
    #  No_TCP; No_SCTP; No_IP; No_IPv6; Prefer_TCP; TLS_old_method;
    #  No_TLS;       # assume transparent security instead of TLS. DTLS is not supported yet (will change in future versions).
    #  Port = 5868;  # The port to connect to
    #  TcTimer = 30;
    #  TwTimer = 30;
    #  ConnectTo = "202.249.37.5";
    #  ConnectTo = "2001:200:903:2::202:1";
    #  TLS_Prio = "NORMAL";
    #  Realm = "realm.net"; # Reject the peer if it does not advertise this realm.
    # Examples:
    #ConnectPeer = "aaa.wide.ad.jp";
    #ConnectPeer = "old.diameter.serv" { TcTimer = 60; TLS_old_method; No_SCTP; Port=3868; } ;
    # AMF IP
    ConnectPeer = "amf.localdomain" { ConnectTo = "10.233.100.202"; No_TLS; };

    ##############################################################
EOF
```

#### 2. 部署 HSS 在 Kubernetes 上

```
cat <<EOF | kubectl create -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nextepc-hss-deployment
  namespace: free5gc
  labels:
    app: nextepc-hss
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nextepc-hss
  template:
    metadata:
      labels:
        app: nextepc-hss
      annotations:
        cni.projectcalico.org/ipAddrs: "[\"10.233.100.203\"]"
    spec:
      hostname: hss
      subdomain: localdomain
      hostAliases:
      - ip: "10.233.100.202"
        hostnames:
        - "amf.localdomain"
      - ip: "10.233.100.203"
        hostnames:
        - "hss.localdomain"
      - ip: "10.233.100.204"
        hostnames:
        - "smf.localdomain"
      - ip: "10.233.100.205"
        hostnames:
        - "pcrf.localdomain"
      - ip: "10.233.100.206"
        hostnames:
        - "upf.localdomain"
      containers:
      - name: nextepc-hss
        image: sufuf3/nextepc-build:latest
        command: ["/bin/bash"]
        args: ["-c", "/usr/src/free5gc/nextepc-hssd"]
        ports:
          - containerPort: 80
            name: api
          - containerPort: 5868
            name: api1
          - containerPort: 3868
            name: api2
        imagePullPolicy: IfNotPresent
        env:
          - name: DB_URI
            value: mongodb://mongo-external.free5gc.svc.cluster.local/nextepc
        volumeMounts:
        - name: config-volume
          mountPath: /usr/src/free5gc/install/etc/free5gc/free5gc.conf
          subPath: free5gc.conf
        - name: hss-volume
          mountPath: /usr/src/free5gc/install/etc/free5gc/hss.conf
          subPath: hss.conf
        - name: freediameter-volume
          mountPath: /usr/src/free5gc/install/etc/free5gc/freeDiameter/hss.conf
          subPath: hss.conf
        securityContext:
          privileged: true
          capabilities:
            add: ["NET_ADMIN", "SYS_TIME"]
      securityContext:
        sysctls:
          - name: net.ipv6.conf.all.disable_ipv6
            value: "0"
      volumes:
        - name: config-volume
          configMap:
            name: free5gc-config
            items:
            - key: config.file
              path: free5gc.conf
        - name: hss-volume
          configMap:
            name: hss-config
            items:
            - key: config.file
              path: hss.conf
        - name: freediameter-volume
          configMap:
            name: freediameter-hss-config
            items:
            - key: config.file
              path: hss.conf
---
apiVersion: v1
kind: Service
metadata:
  name: hss
  labels:
    app: hss
  namespace: free5gc
spec:
  ports:
  - port: 80
    targetPort: 80
  selector:
    app: hss
EOF
```

### 4. 部署 SMF 在 Kubernetes 上

#### 1. 設定 SMF configmap

```
cat <<EOF | kubectl create -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: freediameter-smf-config
  namespace: free5gc
data:
  config.file: |
    # This is a sample configuration file for freeDiameter daemon.

    # Most of the options can be omitted, as they default to reasonable values.
    # Only TLS-related options must be configured properly in usual setups.

    # It is possible to use "include" keyword to import additional files
    # e.g.: include "/etc/freeDiameter.d/*.conf"
    # This is exactly equivalent as copy & paste the content of the included file(s)
    # where the "include" keyword is found.


    ##############################################################
    ##  Peer identity and realm

    # The Diameter Identity of this daemon.
    # This must be a valid FQDN that resolves to the local host.
    # Default: hostname's FQDN
    #Identity = "aaa.koganei.freediameter.net";
    Identity = "smf.localdomain";

    # The Diameter Realm of this daemon.
    # Default: the domain part of Identity (after the first dot).
    #Realm = "koganei.freediameter.net";
    Realm = "localdomain";

    ##############################################################
    ##  Transport protocol configuration

    # The port this peer is listening on for incoming connections (TCP and SCTP).
    # Default: 3868. Use 0 to disable.
    #Port = 3868;

    # The port this peer is listening on for incoming TLS-protected connections (TCP and SCTP).
    # See TLS_old_method for more information about TLS flavours.
    # Note: we use TLS/SCTP instead of DTLS/SCTP at the moment. This will change in future version of freeDiameter.
    # Default: 5868. Use 0 to disable.
    #SecPort = 5868;

    # Use RFC3588 method for TLS protection, where TLS is negociated after CER/CEA exchange is completed
    # on the unsecure connection. The alternative is RFC6733 mechanism, where TLS protects also the
    # CER/CEA exchange on a dedicated secure port.
    # This parameter only affects outgoing connections.
    # The setting can be also defined per-peer (see Peers configuration section).
    # Default: use RFC6733 method with separate port for TLS.
    #TLS_old_method;

    # Disable use of TCP protocol (only listen and connect over SCTP)
    # Default : TCP enabled
    #No_TCP;

    # Disable use of SCTP protocol (only listen and connect over TCP)
    # Default : SCTP enabled
    #No_SCTP;
    No_SCTP;
    # This option is ignored if freeDiameter is compiled with DISABLE_SCTP option.

    # Prefer TCP instead of SCTP for establishing new connections.
    # This setting may be overwritten per peer in peer configuration blocs.
    # Default : SCTP is attempted first.
    #Prefer_TCP;

    # Default number of streams per SCTP associations.
    # This setting may be overwritten per peer basis.
    # Default : 30 streams
    #SCTP_streams = 30;

    ##############################################################
    ##  Endpoint configuration

    # Disable use of IP addresses (only IPv6)
    # Default : IP enabled
    #No_IP;

    # Disable use of IPv6 addresses (only IP)
    # Default : IPv6 enabled
    #No_IPv6;

    # Specify local addresses the server must bind to
    # Default : listen on all addresses available.
    #ListenOn = "202.249.37.5";
    #ListenOn = "2001:200:903:2::202:1";
    #ListenOn = "fe80::21c:5ff:fe98:7d62%eth0";
    ListenOn = "10.233.100.204";


    ##############################################################
    ##  Server configuration

    # How many Diameter peers are allowed to be connecting at the same time ?
    # This parameter limits the number of incoming connections from the time
    # the connection is accepted until the first CER is received.
    # Default: 5 unidentified clients in paralel.
    #ThreadsPerServer = 5;

    ##############################################################
    ##  TLS Configuration

    # TLS is managed by the GNUTLS library in the freeDiameter daemon.
    # You may find more information about parameters and special behaviors
    # in the relevant documentation.
    # http://www.gnu.org/software/gnutls/manual/

    # Credentials of the local peer
    # The X509 certificate and private key file to use for the local peer.
    # The files must contain PKCS-1 encoded RSA key, in PEM format.
    # (These parameters are passed to gnutls_certificate_set_x509_key_file function)
    # Default : NO DEFAULT
    #TLS_Cred = "<x509 certif file.PEM>" , "<x509 private key file.PEM>";
    #TLS_Cred = "/etc/ssl/certs/freeDiameter.pem", "/etc/ssl/private/freeDiameter.key";
    TLS_Cred = "/usr/src/free5gc/install/etc/free5gc/freeDiameter/smf.cert.pem", "/usr/src/free5gc/install/etc/free5gc/freeDiameter/smf.key.pem";

    # Certificate authority / trust anchors
    # The file containing the list of trusted Certificate Authorities (PEM list)
    # (This parameter is passed to gnutls_certificate_set_x509_trust_file function)
    # The directive can appear several times to specify several files.
    # Default : GNUTLS default behavior
    #TLS_CA = "<file.PEM>";
    TLS_CA = "/usr/src/free5gc/install/etc/free5gc/freeDiameter/cacert.pem";

    # Certificate Revocation List file
    # The information about revoked certificates.
    # The file contains a list of trusted CRLs in PEM format. They should have been verified before.
    # (This parameter is passed to gnutls_certificate_set_x509_crl_file function)
    # Note: openssl CRL format might have interoperability issue with GNUTLS format.
    # Default : GNUTLS default behavior
    #TLS_CRL = "<file.PEM>";

    # GNU TLS Priority string
    # This string allows to configure the behavior of GNUTLS key exchanges
    # algorithms. See gnutls_priority_init function documentation for information.
    # You should also refer to the Diameter required TLS support here:
    #   http://tools.ietf.org/html/rfc6733#section-13.1
    # Default : "NORMAL"
    # Example: TLS_Prio = "NONE:+VERS-TLS1.1:+AES-128-CBC:+RSA:+SHA1:+COMP-NULL";
    #TLS_Prio = "NORMAL";

    # Diffie-Hellman parameters size
    # Set the number of bits for generated DH parameters
    # Valid value should be 768, 1024, 2048, 3072 or 4096.
    # (This parameter is passed to gnutls_dh_params_generate2 function,
    # it usually should match RSA key size)
    # Default : 1024
    #TLS_DH_Bits = 1024;

    # Alternatively, you can specify a file to load the PKCS#3 encoded
    # DH parameters directly from. This accelerates the daemon start
    # but is slightly less secure. If this file is provided, the
    # TLS_DH_Bits parameters has no effect.
    # Default : no default.
    #TLS_DH_File = "<file.PEM>";


    ##############################################################
    ##  Timers configuration

    # The Tc timer of this peer.
    # It is the delay before a new attempt is made to reconnect a disconnected peer.
    # The value is expressed in seconds. The recommended value is 30 seconds.
    # Default: 30
    #TcTimer = 30;

    # The Tw timer of this peer.
    # It is the delay before a watchdog message is sent, as described in RFC 3539.
    # The value is expressed in seconds. The default value is 30 seconds. Value must
    # be greater or equal to 6 seconds. See details in the RFC.
    # Default: 30
    #TwTimer = 30;

    ##############################################################
    ##  Applications configuration

    # Disable the relaying of Diameter messages?
    # For messages not handled locally, the default behavior is to forward the
    # message to another peer if any is available, according to the routing
    # algorithms. In addition the "0xffffff" application is advertised in CER/CEA
    # exchanges.
    # Default: Relaying is enabled.
    #NoRelay;

    # Number of server threads that can handle incoming messages at the same time.
    # Default: 4
    #AppServThreads = 4;

    # Other applications are configured by loaded extensions.

    ##############################################################
    ##  Extensions configuration

    #  The freeDiameter framework merely provides support for
    # Diameter Base Protocol. The specific application behaviors,
    # as well as advanced functions, are provided
    # by loadable extensions (plug-ins).
    #  These extensions may in addition receive the name of a
    # configuration file, the format of which is extension-specific.
    #
    # Format:
    #LoadExtension = "/path/to/extension" [ : "/optional/configuration/file" ] ;
    #
    # Examples:
    #LoadExtension = "extensions/sample.fdx";
    #LoadExtension = "extensions/sample.fdx":"conf/sample.conf";

    # Extensions are named as follow:
    # dict_* for extensions that add content to the dictionary definitions.
    # dbg_*  for extensions useful only to retrieve more information on the framework execution.
    # acl_*  : Access control list, to control which peers are allowed to connect.
    # rt_*   : routing extensions that impact how messages are forwarded to other peers.
    # app_*  : applications, these extensions usually register callbacks to handle specific messages.
    # test_* : dummy extensions that are useful only in testing environments.


    # The dbg_msg_dump.fdx extension allows you to tweak the way freeDiameter displays some
    # information about some events. This extension does not actually use a configuration file
    # but receives directly a parameter in the string passed to the extension. Here are some examples:
    ## LoadExtension = "dbg_msg_dumps.fdx" : "0x1111"; # Removes all default hooks, very quiet even in case of errors.
    ## LoadExtension = "dbg_msg_dumps.fdx" : "0x2222"; # Display all events with few details.
    ## LoadExtension = "dbg_msg_dumps.fdx" : "0x0080"; # Dump complete information about sent and received messages.
    # The four digits respectively control: connections, routing decisions, sent/received messages, errors.
    # The values for each digit are:
    #  0 - default - keep the default behavior
    #  1 - quiet   - remove any specific log
    #  2 - compact - display only a summary of the information
    #  4 - full    - display the complete information on a single long line
    #  8 - tree    - display the complete information in an easier to read format spanning several lines.

    LoadExtension = "dbg_msg_dumps.so" : "0x8888";
    LoadExtension = "dict_rfc5777.so";
    LoadExtension = "dict_mip6i.so";
    LoadExtension = "dict_nasreq.so";
    LoadExtension = "dict_nas_mipv6.so";
    LoadExtension = "dict_dcca.so";
    LoadExtension = "dict_dcca_3gpp.so";


    ##############################################################
    ##  Peers configuration

    #  The local server listens for incoming connections. By default,
    # all unknown connecting peers are rejected. Extensions can override this behavior (e.g., acl_wl).
    #
    #  In addition to incoming connections, the local peer can
    # be configured to establish and maintain connections to some
    # Diameter nodes and allow connections from these nodes.
    #  This is achieved with the ConnectPeer directive described below.
    #
    # Note that the configured Diameter Identity MUST match
    # the information received inside CEA, or the connection will be aborted.
    #
    # Format:
    #ConnectPeer = "diameterid" [ { parameter1; parameter2; ...} ] ;
    # Parameters that can be specified in the peer's parameter list:
    #  No_TCP; No_SCTP; No_IP; No_IPv6; Prefer_TCP; TLS_old_method;
    #  No_TLS;       # assume transparent security instead of TLS. DTLS is not supported yet (will change in future versions).
    #  Port = 5868;  # The port to connect to
    #  TcTimer = 30;
    #  TwTimer = 30;
    #  ConnectTo = "202.249.37.5";
    #  ConnectTo = "2001:200:903:2::202:1";
    #  TLS_Prio = "NORMAL";
    #  Realm = "realm.net"; # Reject the peer if it does not advertise this realm.
    # Examples:
    #ConnectPeer = "aaa.wide.ad.jp";
    #ConnectPeer = "old.diameter.serv" { TcTimer = 60; TLS_old_method; No_SCTP; Port=3868; } ;
    # PCRF IP
    ConnectPeer = "pcrf.localdomain" { ConnectTo = "10.233.100.205"; No_TLS; };


    ##############################################################
EOF
```

#### 2. 部署 SMF 在 Kubernetes 上

```
cat <<EOF | kubectl create -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: smf-deployment
  namespace: free5gc
  labels:
    app: smf
spec:
  replicas: 1
  selector:
    matchLabels:
      app: smf
  template:
    metadata:
      labels:
        app: smf
      annotations:
        cni.projectcalico.org/ipAddrs: "[\"10.233.100.204\"]"
    spec:
      hostname: smf
      subdomain: localdomain
      hostAliases:
      - ip: "10.233.100.202"
        hostnames:
        - "amf.localdomain"
      - ip: "10.233.100.203"
        hostnames:
        - "hss.localdomain"
      - ip: "10.233.100.204"
        hostnames:
        - "smf.localdomain"
      - ip: "10.233.100.205"
        hostnames:
        - "pcrf.localdomain"
      - ip: "10.233.100.206"
        hostnames:
        - "upf.localdomain"
      containers:
      - name: smf-container
        image: sufuf3/nextepc-build:latest
        command: ["/bin/bash"]
        #args: ["-c", "sleep 36000"]
        args: ["-c", "/usr/src/free5gc/free5gc-smfd"]
        #ports:
          #- containerPort: 8080
          #  name: api
          #- containerPort: 5868
          #  name: api1
          #- containerPort: 3868
          #  name: api2
        #imagePullPolicy: Always
        imagePullPolicy: IfNotPresent
        env:
          - name: DB_URI
            value: mongodb://mongo-external.free5gc.svc.cluster.local/nextepc
        volumeMounts:
        - name: config-volume
          mountPath: /usr/src/free5gc/install/etc/free5gc/free5gc.conf
          subPath: free5gc.conf
        - name: freediameter-volume
          mountPath: /usr/src/free5gc/install/etc/free5gc/freeDiameter/smf.conf
          subPath: smf.conf
        securityContext:
          privileged: true
          capabilities:
            add: ["NET_ADMIN", "SYS_TIME"]
      securityContext:
        sysctls:
          - name: net.ipv6.conf.all.disable_ipv6
            value: "0"
      volumes:
        - name: config-volume
          configMap:
            name: free5gc-config
            items:
            - key: config.file
              path: free5gc.conf
        - name: freediameter-volume
          configMap:
            name: freediameter-smf-config
            items:
            - key: config.file
              path: smf.conf
---
apiVersion: v1
kind: Service
metadata:
  labels:
    environment: testing
    service: smf
  name: smf
  namespace: free5gc
spec:
  externalTrafficPolicy: Cluster
  ports:
  - name: smf
    nodePort: 31780
    port: 8080
    protocol: TCP
    targetPort: 8080
  selector:
    service: smf
  sessionAffinity: None
  type: NodePort
EOF
```

### 5. 部署 PCRF 在 Kubernetes 上

#### 1. 設定 PCRF configmap

請參考 https://hackmd.io/s/S1baJOeEE#Setup-PCRF

```
cat <<EOF | kubectl create -f -
apiVersion: v1
kind: ConfigMap
metadata:
  name: freediameter-pcrf-config
  namespace: free5gc
data:
  config.file: |
    # This is a sample configuration file for freeDiameter daemon.

    # Most of the options can be omitted, as they default to reasonable values.
    # Only TLS-related options must be configured properly in usual setups.

    # It is possible to use "include" keyword to import additional files
    # e.g.: include "/etc/freeDiameter.d/*.conf"
    # This is exactly equivalent as copy & paste the content of the included file(s)
    # where the "include" keyword is found.


    ##############################################################
    ##  Peer identity and realm

    # The Diameter Identity of this daemon.
    # This must be a valid FQDN that resolves to the local host.
    # Default: hostname's FQDN
    #Identity = "aaa.koganei.freediameter.net";
    Identity = "pcrf.localdomain";

    # The Diameter Realm of this daemon.
    # Default: the domain part of Identity (after the first dot).
    #Realm = "koganei.freediameter.net";
    Realm = "localdomain";

    ##############################################################
    ##  Transport protocol configuration

    # The port this peer is listening on for incoming connections (TCP and SCTP).
    # Default: 3868. Use 0 to disable.
    #Port = 3868;

    # The port this peer is listening on for incoming TLS-protected connections (TCP and SCTP).
    # See TLS_old_method for more information about TLS flavours.
    # Note: we use TLS/SCTP instead of DTLS/SCTP at the moment. This will change in future version of freeDiameter.
    # Default: 5868. Use 0 to disable.
    #SecPort = 5868;

    # Use RFC3588 method for TLS protection, where TLS is negociated after CER/CEA exchange is completed
    # on the unsecure connection. The alternative is RFC6733 mechanism, where TLS protects also the
    # CER/CEA exchange on a dedicated secure port.
    # This parameter only affects outgoing connections.
    # The setting can be also defined per-peer (see Peers configuration section).
    # Default: use RFC6733 method with separate port for TLS.
    #TLS_old_method;

    # Disable use of TCP protocol (only listen and connect over SCTP)
    # Default : TCP enabled
    #No_TCP;

    # Disable use of SCTP protocol (only listen and connect over TCP)
    # Default : SCTP enabled
    #No_SCTP;
    No_SCTP;
    # This option is ignored if freeDiameter is compiled with DISABLE_SCTP option.

    # Prefer TCP instead of SCTP for establishing new connections.
    # This setting may be overwritten per peer in peer configuration blocs.
    # Default : SCTP is attempted first.
    #Prefer_TCP;

    # Default number of streams per SCTP associations.
    # This setting may be overwritten per peer basis.
    # Default : 30 streams
    #SCTP_streams = 30;

    ##############################################################
    ##  Endpoint configuration

    # Disable use of IP addresses (only IPv6)
    # Default : IP enabled
    #No_IP;

    # Disable use of IPv6 addresses (only IP)
    # Default : IPv6 enabled
    #No_IPv6;

    # Specify local addresses the server must bind to
    # Default : listen on all addresses available.
    #ListenOn = "202.249.37.5";
    #ListenOn = "2001:200:903:2::202:1";
    #ListenOn = "fe80::21c:5ff:fe98:7d62%eth0";
    ListenOn = "10.233.100.205";


    ##############################################################
    ##  Server configuration

    # How many Diameter peers are allowed to be connecting at the same time ?
    # This parameter limits the number of incoming connections from the time
    # the connection is accepted until the first CER is received.
    # Default: 5 unidentified clients in paralel.
    #ThreadsPerServer = 5;

    ##############################################################
    ##  TLS Configuration

    # TLS is managed by the GNUTLS library in the freeDiameter daemon.
    # You may find more information about parameters and special behaviors
    # in the relevant documentation.
    # http://www.gnu.org/software/gnutls/manual/

    # Credentials of the local peer
    # The X509 certificate and private key file to use for the local peer.
    # The files must contain PKCS-1 encoded RSA key, in PEM format.
    # (These parameters are passed to gnutls_certificate_set_x509_key_file function)
    # Default : NO DEFAULT
    #TLS_Cred = "<x509 certif file.PEM>" , "<x509 private key file.PEM>";
    #TLS_Cred = "/etc/ssl/certs/freeDiameter.pem", "/etc/ssl/private/freeDiameter.key";
    TLS_Cred = "/usr/src/free5gc/install/etc/free5gc/freeDiameter/pcrf.cert.pem", "/usr/src/free5gc/install/etc/free5gc/freeDiameter/pcrf.key.pem";

    # Certificate authority / trust anchors
    # The file containing the list of trusted Certificate Authorities (PEM list)
    # (This parameter is passed to gnutls_certificate_set_x509_trust_file function)
    # The directive can appear several times to specify several files.
    # Default : GNUTLS default behavior
    #TLS_CA = "<file.PEM>";
    TLS_CA = "/usr/src/free5gc/install/etc/free5gc/freeDiameter/cacert.pem";

    # Certificate Revocation List file
    # The information about revoked certificates.
    # The file contains a list of trusted CRLs in PEM format. They should have been verified before.
    # (This parameter is passed to gnutls_certificate_set_x509_crl_file function)
    # Note: openssl CRL format might have interoperability issue with GNUTLS format.
    # Default : GNUTLS default behavior
    #TLS_CRL = "<file.PEM>";

    # GNU TLS Priority string
    # This string allows to configure the behavior of GNUTLS key exchanges
    # algorithms. See gnutls_priority_init function documentation for information.
    # You should also refer to the Diameter required TLS support here:
    #   http://tools.ietf.org/html/rfc6733#section-13.1
    # Default : "NORMAL"
    # Example: TLS_Prio = "NONE:+VERS-TLS1.1:+AES-128-CBC:+RSA:+SHA1:+COMP-NULL";
    #TLS_Prio = "NORMAL";

    # Diffie-Hellman parameters size
    # Set the number of bits for generated DH parameters
    # Valid value should be 768, 1024, 2048, 3072 or 4096.
    # (This parameter is passed to gnutls_dh_params_generate2 function,
    # it usually should match RSA key size)
    # Default : 1024
    #TLS_DH_Bits = 1024;

    # Alternatively, you can specify a file to load the PKCS#3 encoded
    # DH parameters directly from. This accelerates the daemon start
    # but is slightly less secure. If this file is provided, the
    # TLS_DH_Bits parameters has no effect.
    # Default : no default.
    #TLS_DH_File = "<file.PEM>";


    ##############################################################
    ##  Timers configuration

    # The Tc timer of this peer.
    # It is the delay before a new attempt is made to reconnect a disconnected peer.
    # The value is expressed in seconds. The recommended value is 30 seconds.
    # Default: 30
    #TcTimer = 30;

    # The Tw timer of this peer.
    # It is the delay before a watchdog message is sent, as described in RFC 3539.
    # The value is expressed in seconds. The default value is 30 seconds. Value must
    # be greater or equal to 6 seconds. See details in the RFC.
    # Default: 30
    #TwTimer = 30;

    ##############################################################
    ##  Applications configuration

    # Disable the relaying of Diameter messages?
    # For messages not handled locally, the default behavior is to forward the
    # message to another peer if any is available, according to the routing
    # algorithms. In addition the "0xffffff" application is advertised in CER/CEA
    # exchanges.
    # Default: Relaying is enabled.
    #NoRelay;

    # Number of server threads that can handle incoming messages at the same time.
    # Default: 4
    #AppServThreads = 4;

    # Other applications are configured by loaded extensions.

    ##############################################################
    ##  Extensions configuration

    #  The freeDiameter framework merely provides support for
    # Diameter Base Protocol. The specific application behaviors,
    # as well as advanced functions, are provided
    # by loadable extensions (plug-ins).
    #  These extensions may in addition receive the name of a
    # configuration file, the format of which is extension-specific.
    #
    # Format:
    #LoadExtension = "/path/to/extension" [ : "/optional/configuration/file" ] ;
    #
    # Examples:
    #LoadExtension = "extensions/sample.fdx";
    #LoadExtension = "extensions/sample.fdx":"conf/sample.conf";

    # Extensions are named as follow:
    # dict_* for extensions that add content to the dictionary definitions.
    # dbg_*  for extensions useful only to retrieve more information on the framework execution.
    # acl_*  : Access control list, to control which peers are allowed to connect.
    # rt_*   : routing extensions that impact how messages are forwarded to other peers.
    # app_*  : applications, these extensions usually register callbacks to handle specific messages.
    # test_* : dummy extensions that are useful only in testing environments.


    # The dbg_msg_dump.fdx extension allows you to tweak the way freeDiameter displays some
    # information about some events. This extension does not actually use a configuration file
    # but receives directly a parameter in the string passed to the extension. Here are some examples:
    ## LoadExtension = "dbg_msg_dumps.fdx" : "0x1111"; # Removes all default hooks, very quiet even in case of errors.
    ## LoadExtension = "dbg_msg_dumps.fdx" : "0x2222"; # Display all events with few details.
    ## LoadExtension = "dbg_msg_dumps.fdx" : "0x0080"; # Dump complete information about sent and received messages.
    # The four digits respectively control: connections, routing decisions, sent/received messages, errors.
    # The values for each digit are:
    #  0 - default - keep the default behavior
    #  1 - quiet   - remove any specific log
    #  2 - compact - display only a summary of the information
    #  4 - full    - display the complete information on a single long line
    #  8 - tree    - display the complete information in an easier to read format spanning several lines.

    LoadExtension = "dbg_msg_dumps.so" : "0x8888";
    LoadExtension = "dict_rfc5777.so";
    LoadExtension = "dict_mip6i.so";
    LoadExtension = "dict_nasreq.so";
    LoadExtension = "dict_nas_mipv6.so";
    LoadExtension = "dict_dcca.so";
    LoadExtension = "dict_dcca_3gpp.so";


    ##############################################################
    ##  Peers configuration

    #  The local server listens for incoming connections. By default,
    # all unknown connecting peers are rejected. Extensions can override this behavior (e.g., acl_wl).
    #
    #  In addition to incoming connections, the local peer can
    # be configured to establish and maintain connections to some
    # Diameter nodes and allow connections from these nodes.
    #  This is achieved with the ConnectPeer directive described below.
    #
    # Note that the configured Diameter Identity MUST match
    # the information received inside CEA, or the connection will be aborted.
    #
    # Format:
    #ConnectPeer = "diameterid" [ { parameter1; parameter2; ...} ] ;
    # Parameters that can be specified in the peer's parameter list:
    #  No_TCP; No_SCTP; No_IP; No_IPv6; Prefer_TCP; TLS_old_method;
    #  No_TLS;       # assume transparent security instead of TLS. DTLS is not supported yet (will change in future versions).
    #  Port = 5868;  # The port to connect to
    #  TcTimer = 30;
    #  TwTimer = 30;
    #  ConnectTo = "202.249.37.5";
    #  ConnectTo = "2001:200:903:2::202:1";
    #  TLS_Prio = "NORMAL";
    #  Realm = "realm.net"; # Reject the peer if it does not advertise this realm.
    # Examples:
    #ConnectPeer = "aaa.wide.ad.jp";
    #ConnectPeer = "old.diameter.serv" { TcTimer = 60; TLS_old_method; No_SCTP; Port=3868; } ;
    # SMF IP
    ConnectPeer = "smf.localdomain" { ConnectTo = "10.233.100.204"; No_TLS; };

    ##############################################################
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: pcrf-config
  namespace: free5gc
data:
  config.file: |
    ### For reference, see `free5gc.conf`
    db_uri: mongodb://mongo-external.free5gc.svc.cluster.local/free5gc

    logger:
      file: /usr/src/free5gc/install/var/log/free5gc/pcrf.log
      trace:
        app: 1
        diameter: 1

    parameter:

    pcrf:
      freeDiameter: pcrf.conf
EOF
```

#### 2. 部署 PCRF 在 Kubernetes 上

```
cat <<EOF | kubectl create -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: pcrf-nextepc-deployment
  namespace: free5gc
  labels:
    app: nextepc-pcrf
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nextepc-pcrf
  template:
    metadata:
      labels:
        app: nextepc-pcrf
      annotations:
        cni.projectcalico.org/ipAddrs: "[\"10.233.100.205\"]"
    spec:
      hostname: pcrf
      subdomain: localdomain
      hostAliases:
      - ip: "10.233.100.202"
        hostnames:
        - "amf.localdomain"
      - ip: "10.233.100.203"
        hostnames:
        - "hss.localdomain"
      - ip: "10.233.100.204"
        hostnames:
        - "smf.localdomain"
      - ip: "10.233.100.205"
        hostnames:
        - "pcrf.localdomain"
      - ip: "10.233.100.206"
        hostnames:
        - "upf.localdomain"
      containers:
      - name: nextepc-pcrf
        image: sufuf3/nextepc-build:latest
        command: ["/bin/bash"]
        args: ["-c", "/usr/src/free5gc/nextepc-pcrfd"]
        ports:
          - containerPort: 80
            name: api
          - containerPort: 5868
            name: api1
          - containerPort: 3868
            name: api2
        imagePullPolicy: IfNotPresent
        env:
          - name: DB_URI
            value: mongodb://mongo-external.free5gc.svc.cluster.local/nextepc
        volumeMounts:
        - name: config-volume
          mountPath: /usr/src/free5gc/install/etc/free5gc/free5gc.conf
          subPath: free5gc.conf
        - name: freediameter-volume
          mountPath: /usr/src/free5gc/install/etc/free5gc/freeDiameter/pcrf.conf
          subPath: pcrf.conf
        securityContext:
          privileged: true
          capabilities:
            add: ["NET_ADMIN", "SYS_TIME"]
      securityContext:
        sysctls:
          - name: net.ipv6.conf.all.disable_ipv6
            value: "0"
      volumes:
        - name: config-volume
          configMap:
            name: free5gc-config
            items:
            - key: config.file
              path: free5gc.conf
        - name: freediameter-volume
          configMap:
            name: freediameter-pcrf-config
            items:
            - key: config.file
              path: pcrf.conf
---
apiVersion: v1
kind: Service
metadata:
  name: pcrf
  labels:
    app: pcrf
  namespace: free5gc
spec:
  ports:
  - port: 80
    targetPort: 80
  selector:
    app: pcrf
EOF
```

### 6. 部署 UPF 在 Kubernetes 上

```
cat <<EOF | kubectl create -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: upf-deployment
  namespace: free5gc
  labels:
    app: upf
spec:
  replicas: 1
  selector:
    matchLabels:
      app: upf
  template:
    metadata:
      labels:
        app: upf
      annotations:
        cni.projectcalico.org/ipAddrs: "[\"10.233.100.206\"]"
        k8s.v1.cni.cncf.io/networks: sriov-net1
    spec:
      hostname: upf
      subdomain: localdomain
      hostAliases:
      - ip: "10.233.100.202"
        hostnames:
        - "amf.localdomain"
      - ip: "10.233.100.203"
        hostnames:
        - "hss.localdomain"
      - ip: "10.233.100.204"
        hostnames:
        - "smf.localdomain"
      - ip: "10.233.100.205"
        hostnames:
        - "pcrf.localdomain"
      - ip: "10.233.100.206"
        hostnames:
        - "upf.localdomain"
      containers:
      - name: upf-container
        image: sufuf3/nextepc-build:latest
        command: ["/bin/bash"]
        args: ["-c", "ip addr add 192.188.2.101/24 dev net1 && /root/setup.sh && /usr/src/free5gc/free5gc-upfd"]
        stdin: true
        tty: true
        imagePullPolicy: IfNotPresent
        env:
          - name: DB_URI
            value: mongodb://mongo-external.free5gc.svc.cluster.local/nextepc
        volumeMounts:
        - name: config-volume
          mountPath: /usr/src/free5gc/install/etc/free5gc/free5gc.conf
          subPath: free5gc.conf
        - name: uptun-path
          mountPath: /dev/net/tun
        securityContext:
          privileged: true
          capabilities:
            add: ["NET_ADMIN", "SYS_TIME"]
        resources:
          requests:
            intel.com/sriov_net: '1'
          limits:
            intel.com/sriov_net: '1'
      #hostNetwork: true
      securityContext:
        sysctls:
          - name: net.ipv6.conf.all.disable_ipv6
            value: "0"
      volumes:
        - name: config-volume
          configMap:
            name: free5gc-config
            items:
            - key: config.file
              path: free5gc.conf
        - name: uptun-path
          hostPath:
            path: /dev/net/tun
EOF
```
