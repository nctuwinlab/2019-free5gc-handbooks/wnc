# 2. free5GC v1.0 Docker 映像檔
---

## Docker 映像檔列表

- webui - [nextepc-webui](https://cloud.docker.com/repository/docker/sufuf3/nextepc-webui)：為 free5GC v1.0 的 Web UI 介面。
- nextepc-base: [nextepc-base](https://cloud.docker.com/repository/registry-1.docker.io/sufuf3/nextepc-base)：為 free5GC Function 執行的基本 container 環境。
- nextepc-build: [nextepc-build](https://cloud.docker.com/repository/registry-1.docker.io/sufuf3/nextepc-build)：為編譯 free5GC 程式的 docker 映像檔。

### webui

#### 1. Dockerfile

從 nextepc 參考來的

```
# Modify from: https://raw.githubusercontent.com/acetcom/nextepc/9a86d4cb7cd7b574ec7eb9999c188559c4168bf1/support/docker/webui/Dockerfile

FROM node:carbon

ARG PACKAGE=free5gc

RUN cd /usr/src && git clone https://github.com/sufuf3/kube5GC.git /usr/src/free5gc
RUN cd /usr/src/free5gc && git checkout nctu5gc

WORKDIR /usr/src/free5gc/webui
RUN npm install && \
    npm run build

CMD npm run start

EXPOSE 3000
```

#### 2. Docker 使用方法

首先需要先安裝 MongoDB 並確保 MongoDB_URL 可以被 access。

```
$ docker run -p 3000:3000 -e DB_URI=mongodb://{MongoDB_URL}/nextepc sufuf3/nextepc-webui:v1.0.0
```

#### 3. URL

https://cloud.docker.com/repository/docker/sufuf3/nextepc-webui

### nextepc-base

#### 1. Dockerfile

從 nextepc 參考來的，主要為安裝會編譯 free5GC 的相關套件。

```
# From: https://raw.githubusercontent.com/acetcom/nextepc/7c7cfd1cbf5a0899535ee468096c3471ab48b4a8/support/docker/ubuntu/bionic/base/Dockerfile
ARG dist=ubuntu
ARG tag=latest
#FROM ${dist}:${tag}
FROM golang:1.11-stretch

MAINTAINER Sukchan Lee <acetcom@gmail.com>

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y --no-install-recommends \
        autoconf \
        automake \
        libtool \
        gcc \
        flex \
        bison \
        git \
        libsctp-dev \
        libgnutls28-dev \
        libgcrypt-dev \
        libssl-dev \
        libidn11-dev \
        libmongoc-dev \
        libbson-dev \
        libyaml-dev \
        build-essential \
        iproute2 \
        ca-certificates \
        netbase \
        iptables \
        pkg-config && \
    apt-get clean
```

#### 2. Docker 使用方法

無

#### 3. URL

https://cloud.docker.com/repository/docker/sufuf3/nextepc-base

### nextepc-build

#### 1. Dockerfile

參考 [Source code](https://bitbucket.org/nctu_5g/free5gc-stage-1/src/master/) 的 Part A. Compile Source Code 撰寫的 Dockerfile

```
# Modify from: https://raw.githubusercontent.com/acetcom/nextepc/749d632b86c7faa0550367ec80acc53185812b4a/support/docker/build/Dockerfile

FROM sufuf3/nextepc-base:latest

WORKDIR /root
COPY setup.sh /root
RUN chmod 777 /root/setup.sh

ARG USER=nctucs
ARG REPO=free5gc
ARG BRANCH=master
RUN cd /usr/src && git clone https://bitbucket.org/nctu_5g/free5gc.git /usr/src/free5gc
# Ca't not use v1.0.0, cause the config file is useless for the program
RUN cd /usr/src/free5gc && git checkout da4e100
RUN go get -u -v "github.com/gorilla/mux"
RUN go get -u -v "golang.org/x/net/http2"
RUN go get -u -v "golang.org/x/sys/unix"
ADD https://api.github.com/repos/sufuf3/kube5gc/git/refs/heads/nctu5gc /root/nextepc-ver.json

RUN cd /usr/src/free5gc && \
    autoreconf -iv && \
    ./configure --prefix=`pwd`/install && \
    make -j `nproc` && \
    make install

WORKDIR /usr/src/free5gc
```

並加入 `setup.sh` file

```
#!/bin/sh

if ! grep "uptun" /proc/net/dev > /dev/null; then
    ip tuntap add name uptun mode tun
fi
ip addr del 45.45.0.1/16 dev uptun 2> /dev/null
ip addr add 45.45.0.1/16 dev uptun
#ip addr del cafe::1/64 dev uptun 2> /dev/null
#ip addr add cafe::1/64 dev uptun
ip link set uptun up
sh -c 'echo 1 > /proc/sys/net/ipv4/ip_forward'
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
iptables -I INPUT -i uptun -j ACCEPT
```

#### 2. Docker 使用方法

直接起一整個 free5GC 的方法如下：  

```
$ docker run -e DB_URI=mongodb://{MongoDB_URL}/nextepc sufuf3/nextepc-build:version-1.0.0 --entrypoint ./usr/src/free5gc/free5gc-ngcd
```

若需要使用分開執行 5 個 free5GC 的 Function 則請看 [3. 部署 free5GC CNFs 於 Kubernetes 上之操作](3-Deploy-free5GC-CNFs-on-K8s.md)  

#### 3. URL

https://cloud.docker.com/repository/docker/sufuf3/nextepc-build
