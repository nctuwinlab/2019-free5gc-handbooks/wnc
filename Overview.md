# 前言
---

在本篇報告中，將會簡介如何從 free5GC 的官網安裝步驟轉化成容器化，並建構出 Docker 的映像檔。而在容器化的 free5GC CNFs ，將如何部署在 Kubernetes 上，以及網路上應該如何串接。

* [1. free5GC 容器化設計](1-free5GC-containerization-design.md): 講解 free5GC 容器化的設計。
* [2. free5GC v1.0 Docker 映像檔](2-free5GC-v1.0-docker-images.md)：提供 free5GC v1,0 Docker 的映像檔連結。
* [3. 部署 free5GC CNFs 於 Kubernetes 上之操作](3-Deploy-free5GC-CNFs-on-K8s.md)：提供部署 free5GC CNFs 在 Kubernetes 上之方法，並使用 Multus CNI。
