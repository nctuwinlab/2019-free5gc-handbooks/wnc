# 1. free5GC 容器化設計
---

因應現在 5G 的 Network Functions 都朝虛擬化進行，所以目前都導入  Virtual Network Function 也就是 VNF 的技術，讓 5G 網路可以更靈活和更有效率。  
而因為虛擬化和 Cloud Native 技術的崛起，現在 VNF 都朝 Container base 進行，所以有了 Cloud Native  VNF 也就是 CNF。  
CNF 就是基於 Cloud Native 的 Container 化 VNF ，CNF 有更低的 overhead 以及更高的效能。

因為 CNF 都是一個一個的 container ，所以當 container 數量多時，就需要一個管理 container 的工具。在 container 的管理工具上本報告使用 Kubernetes。

因此，在設計上，我們需要先把 free5GC 官網上在 Host 上安裝的教學轉換成 Container 版本，之後在放到 Kubernetes 上。

- 轉換成 free5GC CNF 的轉換請參考：[2. free5GC v1.0 Docker 映像檔](2-free5GC-v1.0-docker-images.md)
- 將 free5GC CNFs 放到 Kuberentes 上請參考：[3. 部署 free5GC CNFs 於 Kubernetes 上之操作](3-Deploy-free5GC-CNFs-on-K8s.md)
